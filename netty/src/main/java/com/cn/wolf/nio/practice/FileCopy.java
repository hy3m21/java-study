package com.cn.wolf.nio.practice;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author huangy
 * @since 2022/9/9 11:44 上午
 */
public class FileCopy {

    public static void main(String[] args) {
        //准备文件输入输出流，并获取流通道
        try (FileInputStream fis = new FileInputStream("file01.txt");
             FileOutputStream fos = new FileOutputStream("file02.txt");
             FileChannel fisChannel = fis.getChannel();
             FileChannel fosChannel = fos.getChannel()
        ) {
            //准备缓冲区
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            //将数据写入缓冲区
            fisChannel.read(buffer);
            //将缓冲区flip
            buffer.flip();
            fosChannel.write(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
