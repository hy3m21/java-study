package com.cn.wolf.nio.practice;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;

/**
 * @author huangy
 * @since 2022/9/7 8:30 下午
 */
public class NIOServer {

    public static void main(String[] args) throws Exception {
        //创建服务器连接通道
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //获取连接信息
        ServerSocket socket = serverSocketChannel.socket();
        //绑定对应ip和端口
        socket.bind(new InetSocketAddress("localhost", 8888));
        Selector selector = Selector.open();
    }

}
