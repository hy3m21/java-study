package com.cn.wolf.nio.practice;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * nio 练习
 *
 * @author huangy 讲字符串信息写入文件中
 * @since 2022/9/8 11:30 上午
 */
public class NIOFileChannel {

    public static void main(String[] args) {
        //1.准备字符串数据
        String message = "好好学习，天天向上";
        //2.创建文件输出流
        try (FileOutputStream out = new FileOutputStream("file01.txt");
             FileChannel fileChannel = out.getChannel()) {
            //3.创建缓冲区
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            //4.讲数据写入缓冲区
            buffer.put(message.getBytes());
            //5.缓冲区数据flip
            buffer.flip();
            fileChannel.write(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
