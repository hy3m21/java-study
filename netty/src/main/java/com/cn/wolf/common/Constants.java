package com.cn.wolf.common;

/**
 * @author huangy
 * @since 2022/11/27 11:40 上午
 */
public class Constants {

    //主机地址
    public static final String HOST = "127.0.0.1";
    //端口
    public static final int PORT = 6667;

}
