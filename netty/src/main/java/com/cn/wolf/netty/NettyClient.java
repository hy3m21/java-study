package com.cn.wolf.netty;

import com.cn.wolf.common.Constants;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author huangy
 * @since 2023/3/15 22:08
 */
public class NettyClient {

    public static void main(String[] args) throws Exception {
        //事件处理
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup).channel(NioSocketChannel.class).handler(new ChannelInitializer<NioSocketChannel>() {
                @Override
                protected void initChannel(NioSocketChannel channel) {
                    channel.pipeline().addLast(new NettyClientHandler());
                }
            });
            System.out.println("客户端准备完毕...");
            //启动客户端连接服务器
            final ChannelFuture channelFuture = bootstrap.connect(Constants.HOST, Constants.PORT).sync();
            //给关闭通道进行监听
            channelFuture.channel().closeFuture().sync();
        } finally {
            eventLoopGroup.shutdownGracefully();
        }

    }

}
