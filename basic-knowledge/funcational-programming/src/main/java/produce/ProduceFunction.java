package produce;

import java.util.function.Function;

/**
 * @author huangy
 * @since 2022/8/25 10:25 上午
 */

interface FuncSS extends Function<String, String> {
}

@SuppressWarnings("all")
public class ProduceFunction {

    static FuncSS produce(){
        return s -> s.toLowerCase();
    }

    public static void main(String[] args) {
        FuncSS f = produce();
        System.out.println(f.apply("HUANGYUAN"));
    }

}
