package produce;

import java.util.function.Function;

/**
 * @author huangy
 * @since 2022/8/25 10:27 上午
 */

class One {

}

class Two {

}

public class ConsumeFunction {

    static Two consume(Function<One, Two> oneTwo) {
        return oneTwo.apply(new One());
    }

    public static void main(String[] args) {
        Two two = consume(x -> new Two());
    }

}


