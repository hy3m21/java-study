package produce;

import java.util.function.Function;

/**
 * @author huangy
 * @since 2022/8/25 10:33 上午
 */

class I {
    @Override
    public String toString() {
        return "I";
    }
}

class O {
    @Override
    public String toString() {
        return "O";
    }
}

public class TransformFunction {

    /**
     * 接受函数返回函数的方法
     *
     * @param in 函数
     * @return 返回函数
     * @author huangy
     * @since 2022/8/25 10:36 上午
     **/
    static Function<I, O> transformFunction(Function<I, O> in) {
        return in.andThen(o -> {
            System.out.println(o);
            return o;
        });
    }

    public static void main(String[] args) {
        Function<I, O> function = transformFunction(i -> {
            System.out.println(i);
            return new O();
        });
        O o = function.apply(new I());
    }

}
