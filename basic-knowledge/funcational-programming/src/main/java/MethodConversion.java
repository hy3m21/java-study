import java.util.function.BiConsumer;

/**
 * @author huangy
 * @since 2022/8/25 9:56 上午
 */

class In1{}

class In2{}

public class MethodConversion {

    static void accept(In1 in1, In2 in2) {
        System.out.println("accept()");
    }

    static void someOtherName(In1 in1, In2 in2){
        System.out.println("someOtherName()");
    }

    public static void main(String[] args) {
        BiConsumer<In1, In2> bic;
        bic = MethodConversion::accept;
        bic.accept(new In1(), new In2());
        bic = MethodConversion::someOtherName;
        //bic.someOtherName(new In1(), new In2()); 不能这么使用，因为BiConsumer只有accept方法
        bic.accept(new In1(), new In2());
    }

}
