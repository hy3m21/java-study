package closure;

import java.util.function.IntSupplier;

/**
 * @author huangy
 * @since 2022/8/25 11:34 上午
 */
public class Closure2 {

    IntSupplier makeFun(int x) {
        int i = 0;
        return () -> x + i;
    }

    IntSupplier makeFun2(int x) {
        int i = 0;
        //i++ 不支持 Variable used in lambda expression should be final or effectively final
        return () -> x + i;
    }

}
