package closure;

import java.util.function.IntSupplier;

/**
 * @author huangy
 * @since 2022/8/25 10:45 上午
 */
public class Closure1 {
    int i = 10;

    IntSupplier makeFun(int x) {
        return () -> x + i++;
    }

}
