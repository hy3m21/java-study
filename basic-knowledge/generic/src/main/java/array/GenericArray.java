package array;

/**
 * @author huangy
 * @since 2022/8/26 9:50 上午
 */
public class GenericArray<T> {
    private final T[] array;

    @SuppressWarnings("unchecked")
    public GenericArray(int sz) {
        array = (T[]) new Object[sz];
    }

    public void put(int index, T item) {
        array[index] = item;
    }

    public T get(int index) {
        return array[index];
    }

    //暴露潜在表现形式的方法
    public T[] rep() {
        return array;
    }

    public static void main(String[] args) {
        GenericArray<Integer> array = new GenericArray<>(10);
        try {
            Integer[] ia = array.rep();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //这里转换没有问题
        Object[] oa = array.rep();
        System.out.println("hello world");
    }

}
