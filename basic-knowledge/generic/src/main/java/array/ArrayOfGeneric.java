package array;

/**
 * @author huangy
 * @since 2022/8/26 9:22 上午
 */
public class ArrayOfGeneric {

    static final int SIZE = 100;
    static Generic<Integer>[] gia;

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        try {
            gia = (Generic<Integer>[]) new Object[SIZE];
        } catch (ClassCastException e) {
            System.out.println(e.getMessage());
        }
        //运行时是原始类型(已被擦除)
        gia = new Generic[SIZE];
        System.out.println(gia.getClass().getSimpleName());
        gia[0] = new Generic<>();
        //gia[1] = new Object(); 编译错误
        //编译时发现类型不匹配
//        gia[2] = new Generic<Double>();
    }

}
