package array;

import java.lang.reflect.Array;

/**
 * @author huangy
 * @since 2022/8/26 10:54 上午
 */
public class GenericArrayWithTypeToken<T> {

    private T[] array;

    @SuppressWarnings("unchecked")
    public GenericArrayWithTypeToken(Class<T> type, int sz) {
        array = (T[]) Array.newInstance(type, sz);
    }

    public void put(int index, T item) {
        array[index] = item;
    }

    public T get(int index) {
        return array[index];
    }

    //暴露潜在的表达方式
    public T[] rep() {
        return array;
    }

    public static void main(String[] args) {
        GenericArrayWithTypeToken<Integer> token = new GenericArrayWithTypeToken<>(Integer.class, 10);
        for (int i = 0; i < 10; i++) {
            token.put(i, i);
        }
        Integer [] ia = token.rep();
        for (Integer integer : ia) {
            System.out.println(integer);
        }
        System.out.println("hello");
    }

}
