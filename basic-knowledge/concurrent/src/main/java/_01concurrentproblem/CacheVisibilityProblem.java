package _01concurrentproblem;

import java.util.Random;

/**
 * 导致并发问题的因素
 * 因素一
 * 缓存导致的可见性问题
 * 因素二
 * 线程切换带来的原子性问题
 *
 * @author huangy
 * @since 2022/9/11 1:41 下午
 */
public class CacheVisibilityProblem {

    private long count = 0;
    private final Random random = new Random();

    private void add10K() {
        int idx = 0;
        while (idx++ < 10000) {
//            try {
//                //线程sleep随机的秒数，进行优化 但此时的性能是非常差的，并且不能等到正确结果，执行结果19500左右，只是提供了一种思路
//                Thread.sleep(random.nextInt(1));
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            count += 1;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        CacheVisibilityProblem cacheVisibilityProblem = new CacheVisibilityProblem();
        //创建两个线程来完成对count的累加
        Thread th1 = new Thread(cacheVisibilityProblem::add10K);
        Thread th2 = new Thread(cacheVisibilityProblem::add10K);
        //启动连个线程
        th1.start();
        th2.start();
        //等待两个线程执行结束
        th1.join();
        th2.join();
        System.out.println("count值为: " + cacheVisibilityProblem.count);
    }

}

