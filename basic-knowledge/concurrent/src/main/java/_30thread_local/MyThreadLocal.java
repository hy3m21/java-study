package _30thread_local;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author huangy
 * @since 2022/9/19 3:29 下午
 */
public class MyThreadLocal<T> {
    //线程对应变量map
    Map<Thread, T> locals = new ConcurrentHashMap<>();

    //获取线程变量
    T get() {
        return locals.get(Thread.currentThread());
    }

    //设置线程变量
    void set(T t) {
        locals.put(Thread.currentThread(), t);
    }

    public static void main(String[] args) {
    }

}
