import java.util.concurrent.TimeUnit;

/**
 * @author huangy
 * @since 2022/9/5 12:14 下午
 */
public class Timer {
    private final long start = System.nanoTime();
    public long duration() {
        return TimeUnit.NANOSECONDS.toMillis(
                System.nanoTime() - start);
    }
    public static long duration(Runnable test) {
        Timer timer = new Timer();
        test.run();
        return timer.duration();
    }
}
