package _04securemulresources;

/**
 * 一个锁保护多个资源
 *
 * @author huangy
 * @since 2022/9/14 7:02 下午
 */
public class AccountTest {

    //账户余额
    private Integer balance;

    //账户密码
    private String password;

    //取款
    void withdraw(int amt) {
        synchronized (this) {
            if (this.balance > amt) {
                this.balance -= amt;
            }
        }

    }

    //查看余额
    Integer getBalance() {
        synchronized (this) {
            return this.balance;
        }
    }

    //更改密码
    void updatePassword(String pw){
        synchronized (this) {
            this.password = pw;
        }
    }

}
