package _04securemulresources;

/**
 * 保护多个不相干的资源演示
 *
 * @author huangy
 * @since 2022/9/14 3:05 下午
 */
public class Account {

    //锁:保护账户余额
    private final Object balLock = new Object();

    //账户余额
    private Integer balance;

    //锁:保护账户密码
    private final Object pwLock = new Object();

    //账户密码
    private String password;

    //取款
    void withdraw(int amt) {
        synchronized (balLock) {
            if (this.balance > amt) {
                this.balance -= amt;
            }
        }

    }

    //查看余额
    Integer getBalance() {
        synchronized (balLock) {
            return this.balance;
        }
    }

    //更改密码
    void updatePassword(String pw){
        synchronized (pwLock) {
            this.password = pw;
        }
    }

}
