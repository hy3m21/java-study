package _04securemulresources;

/**
 * 一把锁保护多个资源
 *
 * @author huangy
 * @since 2022/9/14 7:12 下午
 */
public class AccountInfo {

    //余额
    private int balance;

    //转账
    void transfer(AccountInfo target, int amt){
        synchronized(AccountInfo.class){
            this.balance -= amt;
            target.balance += amt;
        }

    }

}
