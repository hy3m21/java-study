package _03synchronized;

/**
 * 多个锁保护一个资源的情况
 *
 * @author huangy
 * @since 2022/9/14 11:27 上午
 */
public class SafeCalc {

    static long value = 0;

    synchronized long get() {
        return value;
    }

    synchronized static void addOne() {
        value += 1;
    }

}
