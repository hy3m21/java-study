package _03synchronized;

/**
 * synchronized的使用
 *
 * @author huangy
 * @since 2022/9/14 10:46 上午
 */
public class X {

    //修饰非静态方法
    synchronized void foo() {
        //临界区
    }

    //修饰静态方法
    synchronized static void bar() {
        //临界区
    }

    //修饰代码块
    final Object obj = new Object();

    void baz() {
        synchronized (obj) {
            //临界区
        }
    }

}
