import static java.lang.Thread.sleep;

/**
 * @author huangy
 * @since 2022/9/10 4:24 上午
 */
public class NoVisibility {

    private static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread {
        public void run() {
            while (!ready) {
                System.out.println("hhhh" + number);
                Thread.yield();
            }
            System.out.println("hhh" + number);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new ReaderThread().start();
        sleep(100);
        number = 42;
        ready = true;
    }

}
