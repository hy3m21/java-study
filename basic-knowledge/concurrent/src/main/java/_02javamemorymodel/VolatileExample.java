package _02javamemorymodel;

/**
 * volatile关键字的使用
 *
 * @author huangy
 * @since 2022/9/13 4:29 下午
 */
public class VolatileExample {

    int x = 0;
    volatile boolean v = false;

    public void writer() {
        x = 42;
        v = true;
    }

    public void reader() {
        if (v) {
            System.out.println("x is " + x);
        }
    }

    public static void main(String[] args) {
        VolatileExample voleExample = new VolatileExample();
        Thread t1 = new Thread(voleExample::writer);
        Thread t2 = new Thread(voleExample::reader);
        t1.start();
        t2.start();
    }

}
