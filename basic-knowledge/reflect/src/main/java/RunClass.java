import constants.AnimalType;
import models.Animal;
import models.Silkie;
import org.junit.jupiter.api.Test;
import process.ClassAnimalCreator;
import process.ForNameAnimalCounter;
import process.ForNameAnimalCreator;
import process.TypeCounter;
import proxy.*;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 运行类
 *
 * @author huangy
 * @since 2022/8/11 9:02 上午
 */
@SuppressWarnings("all")
public class RunClass {

    public static void main(String[] args) {
        Silkie silkie = new Silkie();
        silkie.setName("Reyn");
        silkie.setId(1L);
        silkie.setAge(3);
        silkie.setCockscomb("乌");
        silkie.setCode("123456");
        silkie.setGrayValue(23);
        silkie.setType(AnimalType.CommonChicken);
        silkie.setDescription("我就是一只普通的鸡");
        silkie.intro();
    }

    @Test
    public void animalCountTest() {
        new ForNameAnimalCounter().count(new ForNameAnimalCreator());
    }

    @Test
    public void classCountTest() {
        new ForNameAnimalCounter().count(new ClassAnimalCreator());
    }

    @Test
    public void typeCountTest() {
        TypeCounter counter = new TypeCounter(Animal.class);
        new ClassAnimalCreator().stream().limit(30).peek(counter::count).forEach(p ->
                System.out.println(p.getClass().getSimpleName() + " "));
        System.out.println("\n" + counter);
    }

    @Test
    public void proxyTest() {
        Transport transport = new CarTransport();
        InvocationHandler handler = new TransportProxyHandler(transport);
        Transport proxyTransport = (Transport) Proxy.newProxyInstance(transport.getClass().getClassLoader(),
                transport.getClass().getInterfaces(), handler);
        proxyTransport.trans("小汽车");
        proxyTransport.cost();
    }

    @Test
    public void transcationProxyTest() {
        IService service = new StudentServiceImpl();
        InvocationHandler handler = new TransactionProxyHandler(service);
        IService proxyService = (IService) Proxy.newProxyInstance(service.getClass().getClassLoader(),
                service.getClass().getInterfaces(), handler);
        proxyService.add();
        proxyService.remove();
    }
}
