package process;

import models.Animal;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 动物创建器
 *
 * @author huangy
 * @since 2022/8/16 8:26 上午
 */
public abstract class Creator implements Supplier<Animal> {
    private final Random rand = new Random();

    //创建不同类型的动物
    public abstract List<Class<? extends Animal>> types();

    @Override
    public Animal get() {
        //从长度获取随机数
        int n = rand.nextInt(types().size());
        try {
            return types().get(n).getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException
                | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取动物对象集合流
     *
     * @return 流数据
     * @author huangy
     * @since 2022/8/16 10:26 上午
     **/
    public Stream<Animal> stream() {
        return Stream.generate(this);
    }

    /**
     * 获取动物数组
     *
     * @param size 要返回的数据容量
     * @return 动物数组
     */
    public Animal[] array(int size) {
        return stream().limit(size).toArray(Animal[]::new);
    }

    /**
     * 获取动物列表
     *
     * @param size 列表容量
     * @return 动物列表
     * @author huangy
     * @since 2022/8/16 10:29 上午
     **/
    public List<Animal> list(int size) {
        return stream().limit(size).collect(Collectors.toCollection(ArrayList::new));
    }
}
