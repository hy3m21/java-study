package process;

import models.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 字面量动物构建器
 *
 * @author huangy
 * @since 2022/8/16 3:23 下午
 */
public class ClassAnimalCreator extends Creator {

    public static final List<Class<? extends Animal>> ALL_TYPES = Collections.unmodifiableList(Arrays.asList(
            CommonDuck.class,
            BeijingDuck.class,
            NanJingDuck.class,
            CommonChicken.class,
            Silkie.class,
            Berghaan.class,
            Crow.class,
            Sparrow.class,
            Parrot.class
    ));

    //随机生成的类型
    private static final List<Class<? extends Animal>> TYPES = ALL_TYPES.subList(
            ALL_TYPES.indexOf(CommonDuck.class), ALL_TYPES.size());

    @Override
    public List<Class<? extends Animal>> types() {
        return TYPES;
    }
}
