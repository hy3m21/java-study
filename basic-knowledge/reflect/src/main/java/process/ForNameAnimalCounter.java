package process;

import models.*;

import java.util.HashMap;

/**
 * 动物计数器
 *
 * @author huangy
 * @since 2022/8/16 10:44 上午
 */
public class ForNameAnimalCounter {
    static class Counter extends HashMap<String, Integer> {
        /**
         * 计数
         *
         * @param type 给对应类型计数
         * @author huangy
         * @since 2022/8/16 10:48 上午
         **/
        public void count(String type) {
            Integer quantity = get(type);
            if (quantity == null) {
                put(type, 1);
            } else {
                put(type, quantity + 1);
            }
        }
    }

    private final Counter counter = new Counter();

    private void countAnimal(Animal animal) {
        System.out.println(animal.getClass().getName() + "");
        //判断动物的类型,并给对应类型计数
        counter.count("Animal");
        if (animal instanceof Duck) {
            counter.count("Duck");
        }
        if (animal instanceof CommonDuck) {
            counter.count("CommonDuck");
        }
        if (animal instanceof BeijingDuck) {
            counter.count("BeijingDuck");
        }
        if (animal instanceof NanJingDuck) {
            counter.count("NanJingDuck");
        }
        if (animal instanceof Chicken) {
            counter.count("Chicken");
        }
        if (animal instanceof CommonChicken) {
            counter.count("CommonChicken");
        }
        if (animal instanceof Silkie) {
            counter.count("Silkie");
        }
        if (animal instanceof Berghaan) {
            counter.count("Berghaan");
        }
        if (animal instanceof Bird) {
            counter.count("Bird");
        }
        if (animal instanceof Crow) {
            counter.count("Crow");
        }
        if (animal instanceof Sparrow) {
            counter.count("Sparrow");
        }
        if (animal instanceof Parrot) {
            counter.count("Parrot");
        }
    }

    public void count(Creator creator) {
        creator.stream().limit(20).forEach(this::countAnimal);
        System.out.println();
        System.out.println(counter);
    }

}

