package process;

import models.Animal;

import java.util.ArrayList;
import java.util.List;

/**
 * 动物创建 - 类名
 *
 * @author huangy
 * @since 2022/8/16 10:31 上午
 */
public class ForNameAnimalCreator extends Creator {
    private static final List<Class<? extends Animal>> types = new ArrayList<>();
    //要随机生成类型
    private static final String[] typeNames = {
            "models.CommonDuck",
            "models.BeijingDuck",
            "models.NanJingDuck",
            "models.CommonChicken",
            "models.Silkie",
            "models.Berghaan",
            "models.Crow",
            "models.Sparrow",
            "models.Parrot"
    };

    /**
     * 加载动物对应的类
     *
     * @author huangy
     * @since 2022/8/16 10:38 上午
     **/
    @SuppressWarnings("unchecked")
    private static void lorder() {
        try {
            for (String typeName : typeNames) {
                types.add((Class<? extends Animal>) Class.forName(typeName));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static {
        lorder();
    }

    @Override
    public List<Class<? extends Animal>> types() {
        return types;
    }
}
