package optional;

import java.util.Optional;

/**
 * @author huangy
 * @since 2022/8/23 10:50 上午
 */
@SuppressWarnings("all")
public class Person {

    private final Optional<String> first;
    private final Optional<String> last;
    private final Optional<String> address;

    public final boolean empty;

    Person(String first, String last, String address) {
        this.first = Optional.ofNullable(first);
        this.last = Optional.ofNullable(last);
        this.address = Optional.ofNullable(address);
        this.empty = !this.first.isPresent() && !this.last.isPresent() && !this.address.isPresent();
    }

    Person(String first, String last) {
        this(first, last, null);
    }

    Person(String last) {
        this(null, last, null);
    }

    Person() {
        this(null, null, null);
    }

    @Override
    public String toString() {
        if(empty)
            return "<Empty>";
        return (first.orElse("") + " " + last.orElse("") + " " + address.orElse("")).trim();
    }

    public static void main(String[] args) {
        System.out.println(new Person());
        System.out.println(new Person("Smith"));
        System.out.println(new Person("Bob", "Smith"));
        System.out.println(new Person("Bob", "Smith", "11 Degree Lane, Frostbite Falls, MN"));
    }

}
