package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author huangy
 * @since 2022/8/18 4:36 下午
 */
@SuppressWarnings("all")
public class TransportProxyHandler implements InvocationHandler {
    private Object obj;

    public TransportProxyHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("运输前准备");
        Object result = method.invoke(obj, args);
        System.out.println("支付对应的运输费用");
        return result;
    }
}
