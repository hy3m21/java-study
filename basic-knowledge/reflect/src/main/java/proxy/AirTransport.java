package proxy;

/**
 * 飞机运输
 *
 * @author huangy
 * @since 2022/8/18 4:34 下午
 */
public class AirTransport implements Transport {
    @Override
    public void trans(String vehicle) {
        System.out.println("寻找机场");
        System.out.println("准备的交通工具是:" + vehicle);
        System.out.println("飞机降落");
        System.out.println("运输完成");
    }

    @Override
    public void cost() {
        System.out.println("向机场支付费用");
    }
}
