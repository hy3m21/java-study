package proxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author huangy
 * @since 2022/8/18 1:58 下午
 */
@SuppressWarnings("all")
public class ShowerClean implements CleanThing {

    @Override
    public void clean(Object obj, String methodName, Object... params) {
        System.out.println("浴室洗澡前");
        //执行对应类的洗澡方法
        try {
            Method cleanMethod = obj.getClass().getMethod(methodName);
            cleanMethod.invoke(obj, params);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println("浴室洗澡后");
    }
}
