package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author huangy
 * @since 2022/8/18 4:13 下午
 */
@SuppressWarnings("all")
public class CleanProxyHandler implements InvocationHandler {

    private Object obj;

    public CleanProxyHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("清理前操作");
        Object result = method.invoke(obj, args);
        System.out.println("清理后操作");
        return result;
    }
}
