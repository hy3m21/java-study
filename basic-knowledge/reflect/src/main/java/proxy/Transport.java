package proxy;

/**
 * @author huangy
 * @since 2022/8/18 4:24 下午
 */
public interface Transport {

    /**
     * 运输
     *
     * @param vehicle 车
     * @author huangy
     * @since 2022/8/18 4:25 下午
     **/
    void trans(String vehicle);

    /**
     * 费用
     *
     * @author huangy
     * @since 2022/8/22 9:52 上午
     **/
    void cost();
}
