package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 数据库事务动态代理处理器
 *
 * @author huangy
 * @since 2022/8/22 11:06 上午
 */
@SuppressWarnings("all")
public class TransactionProxyHandler implements InvocationHandler {

    private final Object obj;

    public TransactionProxyHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("从数据库连接池获取数据库连接");
        System.out.println("开启事务");
        //执行对应的方法
        Object result = method.invoke(obj, args);
        System.out.println("关闭事务");
        System.out.println("释放数据库连接");
        return result;
    }
}
