package proxy;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 工业清洗
 *
 * @author huangy
 * @since 2022/8/18 2:47 下午
 */
@SuppressWarnings("all")
public class IndustrialCleaning implements CleanThing {

    @Override
    public void clean(Object obj, String methodName, Object... params) {
        System.out.println("工业清洗之前");
        //执行对应类的洗澡方法
        try {
            Method cleanMethod = obj.getClass().getMethod(methodName);
            cleanMethod.invoke(obj, params);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println("工业清洗之后");
    }
}
