package proxy;

/**
 * 清理事物
 *
 * @author huangy
 * @since 2022/8/18 1:56 下午
 */
public interface CleanThing {

    /**
     * 清理方法
     *
     * @param methodName 要执行的方法名称
     * @param params 执行方法的可变参数
     * @author huangy
     * @since 2022/8/18 1:57 下午
     **/
    void clean(Object obj, String methodName,Object... params);

}
