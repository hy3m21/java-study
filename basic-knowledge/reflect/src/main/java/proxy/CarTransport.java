package proxy;

/**
 * @author huangy
 * @since 2022/8/18 4:26 下午
 */
public class CarTransport implements Transport {
    @Override
    public void trans(String vehicle) {
        System.out.println("准备用来运输的车");
        System.out.println("准备的运输工具是:" + vehicle);
        System.out.println("运输完成");
    }

    @Override
    public void cost() {
        System.out.println("向客运中心支付费用");
    }
}
