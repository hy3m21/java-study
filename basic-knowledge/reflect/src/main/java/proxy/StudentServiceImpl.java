package proxy;

/**
 * @author huangy
 * @since 2022/8/22 1:22 下午
 */
public class StudentServiceImpl implements IService {
    @Override
    public void add() {
        System.out.println("添加学生数据");
    }

    @Override
    public void remove() {
        System.out.println("删除学生数据");
    }

    @Override
    public void update() {
        System.out.println("更新学生数据");
    }
}
