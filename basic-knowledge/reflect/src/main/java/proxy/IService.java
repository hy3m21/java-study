package proxy;

/**
 * 基础业务层
 *
 * @author huangy
 * @since 2022/8/22 1:21 下午
 */
public interface  IService {

    void add();

    void remove();

    void update();

}
