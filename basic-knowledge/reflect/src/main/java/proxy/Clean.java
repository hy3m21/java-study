package proxy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 清理方法注解
 *
 * @author huangy
 * @since 2022/8/18 3:50 下午
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Clean {
    //类名
    String className();
    //方法名
    String methodName();
}
