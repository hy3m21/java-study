package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 山鸡
 *
 * @author huangy
 * @since 2022/8/12 4:46 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Berghaan extends Chicken {

    //尾长
    private String tailLength;

    @Override
    public void crow() {
        System.out.println("我是一只山鸡,我非常的野性");
    }

    @Override
    public void intro() {
        super.intro();
        System.out.println("尾长:" + tailLength);
    }
}
