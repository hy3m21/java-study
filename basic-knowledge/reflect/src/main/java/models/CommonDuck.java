package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 普通鸭
 *
 * @author huangy
 * @since 2022/8/15 8:20 上午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CommonDuck extends Duck {

    //伙伴
    private String partner;

    /**
     * 生活在农场
     *
     * @return 生活现状
     * @author huangy
     * @since 2022/8/15 8:24 上午
     **/
    public String liveInFarm() {
        return "我和" + partner + "生活在农场里面";
    }

}
