package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 麻雀
 *
 * @author huangy
 * @since 2022/8/16 10:48 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Sparrow extends Bird {

    //雀斑数
    private Integer numOfFreckles;

    /**
     * 建筑巢穴
     *
     * @author huangy
     * @since 2022/8/16 10:50 下午
     **/
    private void buildNest() {
        System.out.println("在建筑巢穴方面我们是专家");
    }

}
