package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 乌鸦
 *
 * @author huangy
 * @since 2022/8/16 10:45 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Crow extends Bird {
    //黑度
    private Integer blackValue;

    /**
     * 喝水
     *
     * @author huangy
     * @since 2022/8/16 10:47 下午
     **/
    private void drink() {
        System.out.println("我们用石头喝水的");
    }

}
