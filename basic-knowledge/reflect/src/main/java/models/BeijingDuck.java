package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author huangy
 * @since 2022/8/12 5:10 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BeijingDuck extends Duck {

    //北京跳跃
    private String beijingJump;

    /**
     * 在天安门上行走
     *
     * @author huangy
     * @since 2022/8/15 8:14 上午
     **/
    public void walkInChangAnAvenue() {
        System.out.println("我跳着跳着就到了天安门");
    }

}
