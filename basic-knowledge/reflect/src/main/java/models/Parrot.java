package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 鹦鹉
 *
 * @author huangy
 * @since 2022/8/16 10:52 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Parrot extends Bird {

    //格言
    private String aphorism;

    /**
     * 学舌
     *
     * @author huangy
     * @since 2022/8/16 10:54 下午
     **/
    public void talk() {
        System.out.println("我是鹦鹉，我会学舌");
    }

}
