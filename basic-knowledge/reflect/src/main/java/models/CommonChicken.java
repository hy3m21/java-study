package models;

/**
 * @author huangy
 * @since 2022/8/12 4:36 下午
 */

public class CommonChicken extends Chicken {

    /**
     * 下蛋
     *
     * @author huangy
     * @since 2022/8/12 5:01 下午
     **/
    public void layEggs(){
        System.out.println("我下蛋了");
    }

    @Override
    public void crow() {
        System.out.println("ge ge ge get up");
    }

}
