package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 鸟类
 *
 * @author huangy
 * @since 2022/8/16 10:41 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class Bird extends Animal {

    //羽毛
    private String feather;

    @Override
    public void eat() {
        System.out.println("我吃的是:虫子");
    }

    @Override
    public void sleep() {
        System.out.println("我们在鸟窝里面睡觉");
    }

    @Override
    public void clean() {
        System.out.println("鸟儿以雨水洗澡");
    }

    @Override
    public void intro() {
        super.intro();
        System.out.println("羽毛:" + feather);
    }

}
