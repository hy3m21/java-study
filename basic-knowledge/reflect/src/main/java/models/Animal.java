package models;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import constants.AnimalIntroField;
import constants.AnimalType;
import lombok.Data;
import utils.ReflectTool;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 动物类
 *
 * @author huangy
 * @since 2022/8/11 9:05 上午
 */
@Data
public abstract class Animal {

    //id
    private Long id;

    //名称
    private String name;

    //编号
    private String code;

    //年龄
    private Integer age;

    //类型
    private AnimalType type;

    //描述
    private String description;

    /**
     * 吃东西
     *
     * @author huangy
     * @since 2022/8/11 9:32 上午
     **/
    abstract public void eat();

    /**
     * 睡觉
     *
     * @author huangy
     * @since 2022/8/11 10:54 上午
     **/
    abstract public void sleep();

    /**
     * 清洗
     *
     * @author huangy
     * @since 2022/8/18 1:59 下午
     **/
    abstract public void clean();

    /**
     * 自我介绍
     *
     * @author huangy
     * @since 2022/8/11 10:54 上午
     **/
    public void intro() {
        AnimalIntroField[] fields = AnimalIntroField.values();
        for (AnimalIntroField field : fields) {
            //介绍字段的值
            String animalIntroFieldValue = "";
            //通过方法名获取方法
            Method method = ReflectUtil.getMethodByName(this.getClass(), ReflectTool
                    .getMethodNameByPropName(field.getFieldNameEN()));
            //获取方法执行返回值
            if (method != null) {
                try {
                    Object execute = method.invoke(this);
                    if (execute != null) {
                        animalIntroFieldValue = execute.toString();
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            if (StrUtil.isNotBlank(animalIntroFieldValue)) {
                System.out.println(field.getFieldNameCN() + ":" + animalIntroFieldValue);
            }
        }
    }

}
