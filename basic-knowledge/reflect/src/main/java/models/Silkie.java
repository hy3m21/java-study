package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 乌鸡
 *
 * @author huangy
 * @since 2022/8/12 5:02 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Silkie extends Chicken {

    //乌的程度
    private Integer grayValue;

    @Override
    public void crow() {
        System.out.println("呜呜呜呜呜呜");
    }

    @Override
    public void intro() {
        super.intro();
        System.out.println("乌度值:" + grayValue);
    }
}
