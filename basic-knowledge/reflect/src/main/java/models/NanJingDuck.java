package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 南京鸭
 *
 * @author huangy
 * @since 2022/8/15 8:14 上午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NanJingDuck extends Duck {

    //南京飞
    private String nanJingFly;

    /**
     * 南京大学里学习
     *
     * @author huangy
     * @since 2022/8/15 8:18 上午
     **/
    public void studyInNJU() {
        System.out.println("我畅游在南京大学知识的海洋里");
    }

}
