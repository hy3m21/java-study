package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 鸭
 *
 * @author huangy
 * @since 2022/8/12 4:25 下午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class Duck extends Animal {

    //产地
    private String placeOfProduct;

    /**
     * 游泳
     *
     * @author huangy
     * @since 2022/8/12 4:32 下午
     **/
    public void swim() {
        System.out.println("我能在水里游泳，因为我的毛发防水！");
    }

    @Override
    public void eat() {
        System.out.println("我吃的是:谷子");
    }

    @Override
    public void sleep() {
        System.out.println("我在鸭架睡觉");
    }

    @Override
    public void clean() {
        System.out.println("鸭子们在河里洗澡");
    }

    @Override
    public void intro(){
        super.intro();
        System.out.println("我的产地在:" + placeOfProduct);
    }

}
