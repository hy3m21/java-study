package models;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 鸡
 *
 * @author huangy
 * @since 2022/8/11 10:51 上午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class Chicken extends Animal {

    //鸡冠
    private String cockscomb;

    /**
     * 打鸣
     *
     * @author huangy
     * @since 2022/8/12 4:23 下午
     **/
    abstract public void crow();

    @Override
    public void eat() {
        System.out.println("我吃的是:谷子");
    }

    @Override
    public void sleep() {
        System.out.println("我们在鸡笼里面睡觉");
    }

    @Override
    public void clean() {
        System.out.println("鸡们有人帮忙洗澡");
    }

    @Override
    public void intro(){
        super.intro();
        System.out.println("鸡冠:" + cockscomb);
    }

}
