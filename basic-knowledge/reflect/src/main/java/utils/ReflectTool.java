package utils;

/**
 * @author huangy
 * @since 2022/8/11 2:56 下午
 */
public class ReflectTool {

    /**
     * 通过属性名称获取属性值的Get方法名
     * 例如传递 name 返回 getName
     *
     * @param propName 属性名称
     * @return 属性值的方法get方法名
     * @author huangy
     * @since 2022/8/11 2:58 下午
     **/
    public static String getMethodNameByPropName(String propName) {
        return "get" + propName.substring(0, 1).toUpperCase() + propName.substring(1);
    }

    public static void main(String[] args) {
        String methodName = getMethodNameByPropName("abc");
        System.out.println(methodName);
    }

}
