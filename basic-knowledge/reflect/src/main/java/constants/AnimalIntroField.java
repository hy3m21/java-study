package constants;

/**
 * @author huangy
 * @since 2022/8/11 12:10 下午
 */
public enum AnimalIntroField {

    FIELD_NAME(Constants.FieldConstants.FIELD_NAME_CN, Constants.FieldConstants.FIELD_NAME_EN),
    FIELD_CODE(Constants.FieldConstants.FIELD_CODE_CN, Constants.FieldConstants.FIELD_CODE_EN),
    FIELD_AGE(Constants.FieldConstants.FIELD_AGE_CN, Constants.FieldConstants.FIELD_AGE_EN),
    FIELD_TYPE(Constants.FieldConstants.FIELD_TYPE_CN, Constants.FieldConstants.FIELD_TYPE_EN),
    FIELD_DESCRIPTION(Constants.FieldConstants.FIELD_DESCRIPTION_CN, Constants.FieldConstants.FIELD_DESCRIPTION_EN);

    //字段中文名称
    private final String fieldNameCN;
    //字段英文名称
    private final String fieldNameEN;

    AnimalIntroField(String fieldNameCN, String fieldNameEN){
        this.fieldNameCN = fieldNameCN;
        this.fieldNameEN = fieldNameEN;
    }

    public String getFieldNameCN() {
        return fieldNameCN;
    }

    public String getFieldNameEN() {
        return fieldNameEN;
    }

}
