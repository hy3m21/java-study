package constants;

/**
 * 动物类型
 */
public enum AnimalType {

    //普通鸡
    CommonChicken(Constants.ChickenConstants.TYPE_COMMON),
    //乌鸡
    Silkie(Constants.ChickenConstants.TYPE_SILKIE),
    //山鸡
    Berghaan(Constants.ChickenConstants.TYPE_BERGHAAN);

    //类型
    private final String type;

    AnimalType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

}
