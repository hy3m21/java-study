package constants;

/**
 * 通过接口来完成枚举的分组
 *
 * @author huangy
 * @since 2022/8/25 1:53 下午
 **/

public interface Food {

    enum Appetizer implements Food {
        SALAD, SOUP, SPRING_ROLLS
    }

    enum MainCourse implements Food {
        LASAGNE, BURRITO, PAD_THAI
    }

    enum Dessert implements Food {
        TIRAMISU, GELATO, BLACK_FOREST_CAKE
    }

}
