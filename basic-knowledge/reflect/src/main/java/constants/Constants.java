package constants;

/**
 * @author huangy
 * @since 2022/8/11 11:01 上午
 */
public class Constants {

    /**
     * 字段常量
     */
    interface FieldConstants {

        //名称
        String FIELD_NAME_CN = "名称";
        String FIELD_NAME_EN = "name";

        //编号
        String FIELD_CODE_CN = "编码";
        String FIELD_CODE_EN = "code";

        //年龄
        String FIELD_AGE_CN = "年龄";
        String FIELD_AGE_EN = "age";

        //类型
        String FIELD_TYPE_CN = "类型";
        String FIELD_TYPE_EN = "type";

        //描述
        String FIELD_DESCRIPTION_CN = "描述";
        String FIELD_DESCRIPTION_EN = "description";
    }

    /**
     * 鸡常量
     */
    public interface ChickenConstants {
        /**
         * 类型
         */
        //普通鸡
        String TYPE_COMMON = "普通鸡";
        //山鸡
        String TYPE_BERGHAAN = "山鸡";
        //乌鸡
        String TYPE_SILKIE = "乌鸡";
    }

    /**
     * 鸭子常量
     */
    public interface DuckConstants {
        /*
         * 产地
         */
        //北京
        String PLACE_BEIJING = "北京";
        //南京
        String PLACE_NANJING = "南京";
        //全国各地
        String PLACE_EVERY_WHERE = "全国各地";

    }

    /**
     * 反射常量
     */
    public interface ReflectConstants {
        //清洗方法名称
        String CLEAN_METHOD_NAME = "clean";
    }

}
