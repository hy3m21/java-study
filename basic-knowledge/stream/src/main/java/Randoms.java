import java.util.Random;

/**
 * @author huangy
 * @since 2022/8/26 2:09 下午
 */
public class Randoms {

    public static void main(String[] args) {
        new Random(47)
                .ints(5, 20)
                .distinct()
                .limit(7)
                .sorted()
                .forEach(System.out::println);
    }

}
