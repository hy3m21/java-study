import cn.hutool.core.io.resource.ClassPathResource;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author huangy
 * @since 2022/9/2 10:17 上午
 */
public class IOUtil extends BaseUtil {


    /**
     * 通过文件名称从resources中获取文件字符输入流
     *
     * @param fileName 文件名称
     * @return 文件字符流对象
     * @author huangy
     * @since 2022/9/2 10:44 上午
     **/
    public static BufferedReader getBufferedReaderFromFileName(String fileName) throws IOException {
        //获取resources中的文件
        ClassPathResource classPathResource = new ClassPathResource(fileName);
        File file = classPathResource.getFile();
        return Files.newBufferedReader(Paths.get(file.getPath()));
    }

}
