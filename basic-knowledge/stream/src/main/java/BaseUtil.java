/**
 * @author huangy
 * @since 2022/9/2 10:19 上午
 */
public abstract class BaseUtil {

    public BaseUtil() {
        throw new UnsupportedOperationException("不支持初始化");
    }

}
