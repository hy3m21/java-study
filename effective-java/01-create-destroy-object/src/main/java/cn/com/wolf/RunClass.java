package cn.com.wolf;

import cn.com.wolf.builder.Calzone;
import cn.com.wolf.builder.Monster;
import cn.com.wolf.builder.NYPizza;
import cn.com.wolf.staticfactorymethod.Animal;
import cn.com.wolf.staticfactorymethod.AnimalFactory;
import cn.com.wolf.staticfactorymethod.Zoo;
import org.junit.Test;

import java.util.List;

import static cn.com.wolf.builder.Pizza.Topping.*;

/**
 * @author huangy
 * @since 2022/7/23 3:59 上午
 */
public class RunClass {
    public static void main(String[] args) {
        int zooSize = 200;
        Zoo zoo = Zoo.of("武汉动物园", zooSize);
        List<Animal> animals = AnimalFactory.getRandomAnimals(zooSize);
        zoo.setAnimals(animals);
        zoo.showAnimals();
    }

    @Test
    public void builderTest() {
        Monster monster = new Monster.Builder(1L, "小怪物")
                .age(10)
                .aggressivity(100)
                .hp(1000)
                .value(20)
                .build();
        monster.show();
        NYPizza pizza = new NYPizza.Builder(NYPizza.Size.SMALL).addTopping(ONION).addTopping(SAUSAGE).build();
        Calzone calzone = new Calzone.Builder().addTopping(HAM).sauceInside().build();
    }

    @Test
    public void test() {
        String s1 = "a";
        String s2 = s1 + "b";
        String s3 = "ab";
        System.out.println(s2);
        System.out.println(s1.equals(s2));
        System.out.println(s2.equals(s3));
    }
}
