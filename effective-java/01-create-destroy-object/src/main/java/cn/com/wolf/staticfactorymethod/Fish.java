package cn.com.wolf.staticfactorymethod;

/**
 * @author huangy
 * @since 2022/7/25 3:38 下午
 */
public class Fish extends Animal{
    @Override
    public void eat() {
        System.out.println("fish eat");
    }
}
