package cn.com.wolf.builder;

/**
 * 建造者方式，当一个对象的属性有四个及以上的是使用这种方式来初始化对象，给对象赋值
 *
 * @author huangy
 * @since 2022/8/5 10:22 上午
 */
public class Monster {

    private final Long id;
    private final String name;
    private final Integer age;
    private final Integer aggressivity;
    private final Integer hp;
    private final Integer value;

    //内部构造器
    public static class Builder {
        private final Long id;
        private final String name;
        private Integer age = 0;
        private Integer aggressivity = 0;
        private Integer hp = 0;
        private Integer value = 0;

        public Builder(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Builder age(int val) {
            age = val;
            return this;
        }

        public Builder aggressivity(int val) {
            aggressivity = val;
            return this;
        }

        public Builder hp(int val) {
            hp = val;
            return this;
        }

        public Builder value(int val) {
            value = val;
            return this;
        }

        public Monster build() {
            return new Monster(this);
        }

    }

    //私有化构造函数，外部无法直接实例化对象
    private Monster(Builder builder) {
        id = builder.id;
        name = builder.name;
        age = builder.age;
        aggressivity = builder.aggressivity;
        hp = builder.hp;
        value = builder.value;
    }

    public Long getId(){
        return id;
    }

    public void show() {
        System.out.println("名称: " + name);
        System.out.println("年龄: " + age);
        System.out.println("攻击力: " + aggressivity);
        System.out.println("生命值: " + hp);
        System.out.println("价值: " + value);
    }

}
