package cn.com.wolf.staticfactorymethod;

import cn.com.wolf.constant.AnimalConstants;
import cn.com.wolf.constant.AnimalType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 动物工厂
 *
 * @author huangy
 * @since 2022/7/25 3:30 下午
 */
public class AnimalFactory {

    /**
     * 随机
     */
    private static final Random RANDOM = new Random();

    /**
     * 随机获取生成名称
     *
     * @return 随机生成的名称
     * @author huangy
     * @since 2022/7/26 1:40 上午
     **/
    public static String getRandomName() {
        return AnimalConstants.ANIMAL_NAMES[RANDOM.nextInt(AnimalConstants.ANIMAL_NAMES.length)];
    }

    /**
     * 随机生成动物
     *
     * @return Animal 动物信息
     * @author huangy
     * @since 2022/7/25 3:37 下午
     **/
    public static Animal getRandomAnimal() {
        AnimalType[] animalTypes = AnimalType.values();
        AnimalType type = animalTypes[RANDOM.nextInt(animalTypes.length)];
        Animal animal = null;
        try {
            animal = type.getClassType().newInstance();
            //设置动物名称
            animal.setName(getRandomName());
            //设置动物类型
            animal.setType(type);
            //设置动物年龄
            animal.setAge(RANDOM.nextInt(AnimalConstants.ANIMAL_MAX_AGE) + AnimalConstants.ANIMAL_MIN_AGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return animal;
    }

    /**
     * 随机生成动物列表
     *
     * @return 动物列表
     * @author huangy
     * @since 2022/7/26 11:47 上午
     **/
    public static List<Animal> getRandomAnimals(int animalNum) {
        List<Animal> animals = new ArrayList<>();
        for (int i = 0; i < animalNum; i++) {
            animals.add(getRandomAnimal());
        }
        return animals;
    }

}
