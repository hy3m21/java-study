package cn.com.wolf.staticfactorymethod;

/**
 * @author huangy
 * @since 2022/7/25 3:39 下午
 */
public class Bird extends Animal{

    @Override
    public void eat() {
        System.out.println("bird eat");
    }
}
