package cn.com.wolf.staticfactorymethod;

/**
 * @author huangy
 * @since 2022/7/25 3:33 下午
 */
public class Duck extends Animal {

    @Override
    public void eat() {
        System.out.println("duck eat");
    }
}
