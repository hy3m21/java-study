package cn.com.wolf.staticfactorymethod;

import lombok.Data;

import java.util.List;

/**
 * 动物园类
 *
 * @author huangy
 * @since 2022/7/23 4:49 上午
 */
@Data
public class Zoo {

    //名称
    private String name;

    //动物容量 - 能容纳的动物数量
    private Integer size;

    //动物集合
    private List<Animal> animals;

    private Zoo() {
    }

    /**
     * 通过名称创建动物园
     *
     * @author huangy
     * @since 2022/7/26 12:09 下午
     * @param name 动物园名称
     * @return 动物园实例
     **/
    public static Zoo from(String name) {
        Zoo zoo = new Zoo();
        zoo.setName(name);
        return zoo;
    }

    /**
     * 通过名称容量创建动物园
     *
     * @author huangy
     * @since 2022/7/26 12:12 下午
     * @param name 名称
     * @param size 容量
     * @return 动物园实例
     **/
    public static Zoo of(String name, Integer size) {
        Zoo zoo = new Zoo();
        zoo.setSize(size);
        zoo.setName(name);
        return zoo;
    }

    /**
     * 动物园动物展示
     *
     * @author huangy
     * @since 2022/7/23 5:18 上午
     **/
    public void showAnimals() {
        this.animals.forEach(Animal::intro);
    }

}
