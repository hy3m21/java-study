package cn.com.wolf.builder;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

/**
 * Builder模式用于类层次结构
 *
 * @author huangy
 * @since 2022/8/5 11:09 上午
 */
public abstract class Pizza {

    public enum Topping {HAM, MUSHROOM, ONION, PEPPER, SAUSAGE};

    final Set<Topping> toppings;

    abstract static class Builder<T extends Builder<T>> {
        EnumSet<Topping> toppings = EnumSet.noneOf(Topping.class);
        public T addTopping(Topping topping){
            toppings.add(Objects.requireNonNull(topping));
            return self();
        }
        abstract Pizza build();

        //子类必须重写这个方法来返回this
        protected abstract T self();
    }

    Pizza (Builder<?> builder) {
        toppings = builder.toppings.clone();
    }
}
