package cn.com.wolf.staticfactorymethod;

import cn.com.wolf.constant.AnimalType;
import lombok.Data;

/**
 * @author huangy
 * @since 2022/7/21 3:39 下午
 */
@Data
public abstract class Animal {

    //名称
    private String name;

    //年龄
    private Integer age;

    //类型
    private AnimalType type;

    /**
     * 动物简介
     *
     * @author huangy
     * @since 2022/7/23 4:52 上午
     **/
    public void intro() {
        System.out.println("我是一只:" + type.getDescription());
        System.out.println("我的名字是:" + this.getName());
        System.out.println("我今年" + age + "岁");
        this.eat();
        System.out.println("--------------------------------------------------------");
    }

    /**
     * 吃东西
     *
     * @author huangy
     * @since 12:00 下午
     **/
    abstract public void eat();

}
