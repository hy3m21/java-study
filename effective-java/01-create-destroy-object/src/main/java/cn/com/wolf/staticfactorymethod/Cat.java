package cn.com.wolf.staticfactorymethod;

/**
 * @author huangy
 * @since 2022/7/25 3:19 下午
 */
public class Cat extends Animal {
    @Override
    public void eat() {
        System.out.println("cat eat");
    }
}
