package cn.com.wolf.constant;

import cn.com.wolf.staticfactorymethod.*;

public enum AnimalType {
    DOG("狗", AnimalConstants.AnimalCode.DOG, Dog.class),
    CAT("猫", AnimalConstants.AnimalCode.CAT, Cat.class),
    DUCK("鸭子", AnimalConstants.AnimalCode.DUCK, Duck.class),
    FISH("鱼", AnimalConstants.AnimalCode.FISH, Fish.class),
    BIRD("鸟", AnimalConstants.AnimalCode.BIRD, Bird.class);
    private final String description;
    private final String code;
    private final Class<? extends Animal> classType;

    AnimalType(String description, String code, Class<? extends Animal> classType) {
        this.description = description;
        this.code = code;
        this.classType = classType;
    }

    public String getDescription() {
        return this.description;
    }

    public String getCode() {
        return this.code;
    }

    public Class<? extends Animal> getClassType() {
        return this.classType;
    }
}
