package cn.com.wolf.staticfactorymethod;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author huangy
 * @since 2022/7/22 8:19 上午
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Dog extends Animal {

    @Override
    public void eat() {
        System.out.println("dog eat");
    }
}
