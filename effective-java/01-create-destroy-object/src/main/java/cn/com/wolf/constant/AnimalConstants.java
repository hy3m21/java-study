package cn.com.wolf.constant;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnimalConstants {

    //动物数字对应动物类
    public static final Map<Integer, AnimalType> VALUE_TO_ANIMAL_TYPE = new HashMap<>();

    //动物名称数组
    public static final String[] ANIMAL_NAMES = {
            "Tom", "Bob", "Jerry", "Jeff", "John", "Smith",
            "Jack", "Kobe", "Mark", "Barry", "Claire", "John",
            "Jessica", "Elizabeth", "Annie", "Rachel", "Jennifer", "Rose"
    };

    //动物年龄范围
    //最小年龄
    public static final int ANIMAL_MIN_AGE = 0;

    //最大年龄
    public static final int ANIMAL_MAX_AGE = 50;

    //动物类型列表
    public static final List<AnimalType> ANIMAL_TYPES = new ArrayList<>();

    /**
     * 动物编码
     */
    interface AnimalCode {
        //狗
        String DOG = "001";
        //猫
        String CAT = "002";
        //鸭子
        String DUCK = "003";
        //鱼
        String FISH = "004";
        //鸟
        String BIRD = "005";
    }

    //初始化数据
    static {
        //初始化值对应动物类型
        for (AnimalType value : AnimalType.values()) {
            VALUE_TO_ANIMAL_TYPE.put(value.ordinal(), value);
            ANIMAL_TYPES.add(value);
        }
    }

}
