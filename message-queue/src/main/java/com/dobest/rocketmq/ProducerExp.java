package com.dobest.rocketmq;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

/**
 * @author huangy
 * @description 消息生产者
 * @since 2022/11/8 15:31
 **/
public class ProducerExp {

    public static void main(String[] args) throws MQClientException {
        DefaultMQProducer producer = new DefaultMQProducer("java-group");
        producer.setNamesrvAddr("127.0.0.1:9876");
        producer.setInstanceName("producer");
        producer.start();
        try {
            for (int i = 0; i < 10; i++) {
                //每秒发送一次MQ
                Thread.sleep(1000);
                //topic主题名称  tag临时值， body内容
                Message message = new Message("hy-test-001", "TagA", ("huangy-" + i).getBytes());
                SendResult sendResult = producer.send(message);
                System.out.println("来了来了：" + sendResult.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        producer.shutdown();
    }

}
