package com.dobest.json;

import com.alibaba.fastjson2.JSON;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author huangy
 * @description
 * @since 2022/11/23 17:32
 **/
public class JsonTest {

    private static final List<String> apiRote = Arrays.asList(
            //性能参考接口
            "0-0",
            //设置用户客户端信息
            "0-3",
            //获取用户客户端信息
            "0-2",
            //完成任务
            "4-1",
            //居民升级
            "3-1",
            //居民升星
            "3-2",
            //免费契约
            "5-1",
            //黄金契约
            "5-2",
            //闪耀契约
            "5-3",
            //快速获取
            "9-3",
            //使用道具
            "9-1",
            //消耗体力
            "8-1",
            //获取体力
            "8-4",
            //通过炼金关卡
            "8-3",
            //获取粉尘
            "8-3"
    );

    private static final List<String> apiName = Arrays.asList(
            "性能参考接口",
            "设置用户客户端信息",
            "获取用户客户端信息",
            "完成任务",
            "居民升级",
            "居民升星",
            "免费契约",
            "黄金契约",
            "闪耀契约 ",
            "快速获取",
            "使用道具",
            "消耗体力",
            "获取体力",
            "通过炼金关卡",
            "获取粉尘"
    );

    @Test
    public void jsonTest() {
        String clientInfoData = "{datas:{common_storage_data:{\"last_loginTime\":1669193841,\"is_openAdMul\":1,\"reset_data_check_time\":{\"is_openAdMul\":1669219200,\"is_openFirstPay\":1669219200,\"is_openTrading\":1669219200,\"gate_extra_num_reset\":1669219200,\"reset_event_max\":1669219200,\"bachelor_quick_reset\":1669219200,\"daily_week_reset\":1669564800,\"daily_day_reset\":1669237200,\"show_money_hand_num\":1669219200},\"is_openFirstPay\":1,\"is_openTrading\":0,\"sys_open_ids\":[\"3003\",\"501\",\"3011\",\"2015\",\"1002\",\"1003\",\"2007\",\"3015\",\"3001\",\"401\",\"2010\",\"3002\",\"2005\",\"2004\",\"3020\",\"3013\",\"2003\",\"2006\",\"2008\",\"3023\",\"3016\",\"3021\"],\"open_book\":\"1\",\"gate_extra_num_reset\":1,\"bachelor_offline_start_time\":1669193829,\"Upgrade_index\":\"max\",\"task_finished_1\":1,\"task_finished_2\":1,\"reset_event_max\":1,\"bachelor_last_out\":\"1.685120000002E4\",\"bachelor_last_time\":1669169559,\"bachelor_quick_reset\":1,\"bachelor_quick\":{\"free\":1,\"n\":3},\"task_finished_3\":1,\"daily_week_reset\":1,\"daily_day_reset\":1,\"login_reward_gets\":[\"item20016:3\"],\"last_video_time\":1669169873,\"show_money_hand_num\":3,\"task_finished_4\":1,\"__ut\":1669193841}}}";
        Map<String, String> data = JSON.parseObject(clientInfoData, Map.class);
        data.forEach((key, value) -> {
            System.out.println(key);
            System.out.println(value);
        });
    }

    @Test
    public void apiRote2Name() {
        Map<String, String> rote2Name = new HashMap<>();
        for (int i = 0; i < apiRote.size(); i++) {
            rote2Name.put(apiRote.get(i), apiName.get(i));
        }
        String s = JSON.toJSONString(rote2Name);
        System.out.println(s);
    }

    @Test
    public void strToJson() {
        System.out.println(kv2Json("mailId", 1));
    }

    /*
     * @description 键值 转为 json
     * @author huangy
     * @since 2023/3/14 11:07
     * @param k
     * @param v
     * @return java.lang.String
     **/
    private static String kv2Json(Object k, Object v) {
        Map<Object, Object> map = new HashMap<>();
        map.put(k, v);
        return JSON.toJSONString(map);
    }

}
