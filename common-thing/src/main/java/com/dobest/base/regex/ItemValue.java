package com.dobest.base.regex;

import lombok.Data;

/**
 * @author huangy
 * @description
 * @since 2022/10/22 16:56
 **/
@Data
public class ItemValue {

    private int id;

    private int type;

    private int num;

}
