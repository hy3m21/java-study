package com.dobest.base.regex;

import lombok.Data;

/**
 * @author huangy
 * @description 奖励信息
 * @since 2022/10/9 17:34
 **/
@Data
public class Reward {
    /**
     * 序号
     */
    int id;

    /**
     * 奖励类型
     */
    int type;

    /**
     * 奖励数量
     */
    int num;
}
