package com.dobest.base.regex;

import cn.hutool.core.text.CharSequenceUtil;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author huangy
 * @description
 * @since 2022/10/22 17:00
 **/
public class RegTest {

    public static final Map<String, String> OTHER_MAP = new HashMap<>();

    public static final LevelConstants LEVEL_CONSTANTS = LevelConstants.getInstance();

    @Test
    public void noSpecialChar() {
        String pattern = "[`_~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        String a = "sddGk805942jklvmm안녕히계세요にほんごAaÄäOoÖöBbCcDdEeFfGgHhIPpQqRrSsßTtUuÜü";
        a = a.replaceAll(" ", "");
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(a);
        if (matcher.find()) {
            System.out.println("字符串:" + a + ", 包含特殊字符");
        } else {
            System.out.println("字符串:" + a + ", 不包含特殊字符");
        }
    }

    @Test
    public void matchCN() {
        String cn = "我s是요に黄vfsdf远 黃遠";
        String pattern = "[\u4E00-\u9FA5]";
        Pattern p = Pattern.compile(pattern);
        char[] chars = cn.toCharArray();
        Matcher matcher = p.matcher(cn);
        if (matcher.find()) {
            for (int i = 0; i < chars.length; i++) {
                char c = chars[i];
                if (c >= 0x4e00 && c <= 0x9FA5) {
                    System.out.println(chars[i]);
                } else {
                    System.out.println("a");
                }
            }
        }
    }

    public static void main(String[] args) {
        Class<? extends LevelConstants> levelClass = LEVEL_CONSTANTS.getClass();
        Field[] declaredFields = levelClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if (declaredField.getType().equals(int.class)) {
                System.out.println("int 类型");
            } else {
                System.out.println("不是 int 类型");
            }
            System.out.println(declaredField.getType().getTypeName());
            System.out.println(declaredField.getName());
            System.out.println(CharSequenceUtil.toUnderlineCase(declaredField.getName()));
        }
    }
}
