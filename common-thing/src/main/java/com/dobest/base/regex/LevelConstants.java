package com.dobest.base.regex;

/**
 * @author huangy
 * @description
 * @since 2022/10/22 16:15
 **/
public class LevelConstants {

    private static final LevelConstants INSTANCE = new LevelConstants();

    //体力回复上限
    private int magicMax;
    //一点体力回复时间(单位:秒)
    private int magicTime;
    //播放广告获得的体力值(道具+数量)
    private ItemValue adMagic;
    //购买体力所需的道具(道具+数量)
    private ItemValue itemBuy;
    //道具购买获得的体力值(道具+数量)
    private ItemValue itemMagic;
    //每日广告购买体力上限次数
    private int adMagicMax;
    //每日钻石购买体力上限次数
    private int itemMagicMax;
    //炼金结算额外奖励
    private Reward adReward;
    //炼金额外奖励每日最大次数
    private int adRewardMax;
    private LevelConstants() {
    }
    public static LevelConstants getInstance() {
        return INSTANCE;
    }

    public int getMagicMax() {
        return magicMax;
    }

    public void setMagicMax(int magicMax) {
        this.magicMax = magicMax;
    }

    public int getMagicTime() {
        return magicTime;
    }

    public void setMagicTime(int magicTime) {
        this.magicTime = magicTime;
    }

    public ItemValue getAdMagic() {
        return adMagic;
    }

    public void setAdMagic(ItemValue adMagic) {
        this.adMagic = adMagic;
    }

    public ItemValue getItemBuy() {
        return itemBuy;
    }

    public void setItemBuy(ItemValue itemBuy) {
        this.itemBuy = itemBuy;
    }

    public ItemValue getItemMagic() {
        return itemMagic;
    }

    public void setItemMagic(ItemValue itemMagic) {
        this.itemMagic = itemMagic;
    }

    public int getAdMagicMax() {
        return adMagicMax;
    }

    public void setAdMagicMax(int adMagicMax) {
        this.adMagicMax = adMagicMax;
    }

    public int getItemMagicMax() {
        return itemMagicMax;
    }

    public void setItemMagicMax(int itemMagicMax) {
        this.itemMagicMax = itemMagicMax;
    }

    public Reward getAdReward() {
        return adReward;
    }

    public void setAdReward(Reward adReward) {
        this.adReward = adReward;
    }

    public int getAdRewardMax() {
        return adRewardMax;
    }

    public void setAdRewardMax(int adRewardMax) {
        this.adRewardMax = adRewardMax;
    }
}
