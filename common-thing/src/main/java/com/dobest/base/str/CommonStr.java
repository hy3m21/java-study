package com.dobest.base.str;

import cn.hutool.core.text.CharSequenceUtil;
import com.dobest.utils.PrintUtils;
import org.junit.jupiter.api.Test;

/**
 * @author huangy
 * @description
 * @since 2022/11/17 15:15
 **/
public class CommonStr {

    @Test
    public void strFormat() {
        System.out.println(String.format("goblin_client_info_%03d", 101));
    }

    @Test
    public void humpAndUnderline() {
        String str = "a_b";
        System.out.println(CharSequenceUtil.toCamelCase(str));
    }

    @Test
    public void strAndByte() {
        byte[] byteContent = {10, 9, 8, 2, 16, 1, 24, -30, -109, -79, 2};
        byte[] newByte = new byte[byteContent.length];
        for (int i = 0; i < byteContent.length; i++) {
            if (byteContent[i] < 0) {
                newByte[i] = (byte) (byteContent[i] + 256);
            } else {
                newByte[i] = byteContent[i];
            }
        }
        PrintUtils.printArray(newByte);
        String contentStr = new String(byteContent);
        System.out.println(contentStr);
    }

    @Test
    public void strJoin() {
        String test = String.format("{mailId:%s}", 1);
        System.out.println(test);
    }

    @Test
    public void strHash() {
        String uniqId = "6f53d11b-02f2-4e9f-b713-9c77150b82ac";
        System.out.println(String.format("goblin_device_user_%03d", Math.abs(uniqId.hashCode()) % 16 + 1));
    }

}
