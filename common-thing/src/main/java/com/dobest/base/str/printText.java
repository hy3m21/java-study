package com.dobest.base.str;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;

/**
 * @author huangy
 * @description
 * @since 2022/12/7 21:05
 **/
public class printText {

    @Test
    public void printText() {
        try {
            String text = "测试结束";
            Font font = new Font("黑体", Font.PLAIN, 24);
            AffineTransform at = new AffineTransform();
            FontRenderContext frc = new FontRenderContext(at, true, true);
            GlyphVector gv = font.createGlyphVector(frc, text); //要显示的文字(文字的字形)
            Shape shape = gv.getOutline(2, 15);
            int wide = 1000;
            int height = 40;
            boolean[][] view = new boolean[wide][height];
            for (int i = 0; i < wide; i++) {
                for (int j = 0; j < height; j++) {
                    view[i][j] = shape.contains(i, j);
                }
            }
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < wide; i++) {
                    if (view[i][j]) {
                        System.out.print("*");//替换成你喜欢的图案
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
