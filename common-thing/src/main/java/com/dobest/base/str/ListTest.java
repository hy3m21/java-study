package com.dobest.base.str;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author huangy
 * @description
 * @since 2022/10/11 15:48
 **/
public class ListTest {

    @Test
    public void containsTest() {
        List<Integer> DROP_GOLD_LIST = Arrays.asList(
                20001,
                20002,
                20003,
                20004,
                20005,
                20006
        );
        int a = 20002;
        System.out.println(DROP_GOLD_LIST.contains(a));
    }

}
