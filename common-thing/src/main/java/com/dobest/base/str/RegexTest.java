package com.dobest.base.str;

import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author huangy
 * @description
 * @since 2022/10/10 9:29
 **/
public class RegexTest {

    public static void main(String[] args) {
        String pattern = "\\d+";
        String str = "huangy12jf8sk91";
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(str);
        if (matcher.find()) {
            System.out.println(str.substring(0, str.indexOf(matcher.group(0))));
        }
    }

    @Test
    public void matchLetter() {
        String pattern = "[a-zA-Z]+";
        String str = "item11";
        matchPrint(pattern, str);
    }

    @Test
    public void matchNum(){
        String pattern = "^\\d+$";
        String str = "1111";
        matchPrint(pattern, str);
    }

    /*
     * @description 只匹配数字
     * @author huangy
     * @since 2022/10/18 9:51
     * @param pattern
     * @param str
     **/
    private static void matchPrint(String pattern, String str) {
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(str);
        if (matcher.find()) {
            System.out.println(matcher.group());
        } else {
            System.out.println("无法匹配");
        }
    }

}
