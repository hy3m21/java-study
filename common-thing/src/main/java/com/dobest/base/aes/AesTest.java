package com.dobest.base.aes;

import com.alibaba.fastjson2.JSON;
import com.dobest.ServerToken;
import com.dobest.utils.AESCrypt;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huangy
 * @description
 * @since 2023/2/16 15:46
 **/
public class AesTest {

    private static final String AES_KEY = "3B0BFF36E170A2A3";

    //用户id
    private static final List<Long> USER_ID_LIST = new ArrayList<>();

    static {
        for (int i = 0; i < 2000; i++) {
            USER_ID_LIST.add((long) (i + 1));
        }
    }

    @Test
    public void encodeTest() {
        for (Long userId : USER_ID_LIST) {
            System.out.println("userId: " + userId + ", token: " + getToken(userId));
        }
    }

    /*
     * @description 获取token
     * @author huangy
     * @since 2023/2/28 10:47
     * @param userId
     * @return java.lang.String
     **/
    private static String getToken(long userId) {
        ServerToken serverToken = ServerToken.of("goblin", null, userId);
        String content = JSON.toJSONString(serverToken);
        return AESCrypt.encrypt(content, AES_KEY);
    }

}
