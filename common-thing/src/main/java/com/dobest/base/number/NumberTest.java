package com.dobest.base.number;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * @author huangy
 * @description
 * @since 2022/10/25 11:51
 **/
public class NumberTest {

    public static void main(String[] args) {
        double a = 28799.99999999999;
        double c = 28799.999999999999999;
        long b = 28800;
        System.out.println(a == b);
        System.out.println(c == b);
        double interval = 28800 / 60.0;
        System.out.println(interval);
    }

    @Test
    public void str2Long() {
        String numStr = "105";
        long num = Long.parseLong(numStr);
        System.out.println("numStr: " + numStr);
        System.out.println("num: " + num);
    }

    @Test
    public void doubleTest() {
        double a = 0.00015;
        System.out.println(a);
        String b = "-1";
        System.out.println(Integer.parseInt(b));
        System.out.println(getDouble());
    }

    public double getDouble() {
        return 1 + (double) 50 / 100;
    }

    @Test
    public void testStreamInt() {
        List<Integer> numList = Arrays.asList(1, 3, 4, 5, 7, 9, 10);
        IntStream intStream = numList.stream().mapToInt(Integer::intValue);
        System.out.println(intStream.sum());
    }

    @Test
    public void testAtomic() {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        atomicInteger.addAndGet(1);
        atomicInteger.addAndGet(1);
        atomicInteger.addAndGet(1);
        System.out.println(atomicInteger.get());
    }

    @Test
    public void leftMoveSign() {
        System.out.println((1 << 16) + 1);
        System.out.println(1179650 >> 16);
        System.out.println(18 << 16);
    }

    @Test
    public void getPortNum() {
        int portNum = 1179650;
        int cmd = portNum >> 16;
        int subCmd = portNum - (cmd << 16);
        System.out.println("对应路由为: " + cmd + "-" + subCmd);
    }

    @Test
    public void getCmdMerge(){
        int cmd = 24;
        int subCmd = 4;
        int portNum = cmd << 16 + subCmd;
        System.out.println("对应 cmdMerge: " + portNum);
    }

}
