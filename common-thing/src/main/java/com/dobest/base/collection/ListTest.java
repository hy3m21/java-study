package com.dobest.base.collection;

import com.dobest.base.regex.ItemValue;
import com.dobest.utils.PrintUtils;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author huangy
 * @description
 * @since 2022/11/17 16:43
 **/
public class ListTest {

    @Test
    public void listSplit() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 31; i++) {
            list.add(i + 1);
        }
        //进行列表拆分
        List<List<Integer>> lists = Lists.partition(list, 5);
        lists.forEach(listPart -> {
            System.out.println("列表大小" + listPart.size());
            PrintUtils.printList(listPart);
        });
    }

    @Test
    public void removeElement() {
        List<ItemValue> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ItemValue itemValue = new ItemValue();
            itemValue.setId(i);
            itemValue.setNum(1);
            itemValue.setType(1);
            list.add(itemValue);
        }
        ItemValue removeItem = null;
        for (ItemValue itemValue : list) {
            if (itemValue.getId() == 2) {
                removeItem = itemValue;
                break;
            }
        }
        if (removeItem != null) {
            list.remove(removeItem);
        }
        System.out.println(list.size());
    }

    @Test
    public void listSort() {
        List<Long> list = Arrays.asList(1L, 3L, -5L, 80L, 12L, 26L, 10L);
        PrintUtils.printList(list);
        List<Long> collect = list.stream().sorted(Comparator.comparing(Long::longValue).reversed()).collect(Collectors.toList());
        PrintUtils.printList(collect);
    }


}
