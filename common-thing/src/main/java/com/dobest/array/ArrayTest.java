package com.dobest.array;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author huangy
 * @description
 * @since 2023/1/18 15:47
 **/
public class ArrayTest {

    @Test
    public void arrayTest() {
        List<Integer> list1 = Arrays.asList(80001,
                40001, 40002, 80003, 40003, 80002, 40004, 80005, 80004, 40005,
                90001, 10001, 50001, 50002, 10002, 50003, 90002, 50004, 50005,
                50006, 50007, 50008, 50009, 50010, 100001, 20001, 20002, 20003,
                100002, 70001, 70003, 70002, 70005, 70004, 70007, 70006, 70009,
                70008);
        List<Integer> finishCups = list1.stream().sorted().toList();
        int count = 0;
        for (Integer finishCup : finishCups) {
            if (finishCup > 70000 && finishCup < 90000) {
                count++;
            }
            System.out.println(finishCup);
        }
        System.out.println("获取数量:" + count);
    }

}
