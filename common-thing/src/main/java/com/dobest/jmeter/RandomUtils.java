package com.dobest.jmeter;

import java.util.Random;

/**
 * @author huangy
 * @description
 * @since 2022/11/14 14:17
 **/
public class RandomUtils {

    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            long id = RANDOM.nextLong(100000000, 110000000);
            System.out.println(id);
        }
    }

}
