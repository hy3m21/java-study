package com.dobest.lib.interval;

import java.util.List;

/**
 * @author huangy
 * @description 间隔工具类
 * @since 2022/11/7 21:11
 **/
public class IntervalUtils {

    /*
     * @description 通过对应的值获取该值所在的区间
     * @author huangy
     * @param intervals 区间列表
     * @param value 指定的值
     * @return com.dobest.goblin.gameserver.utils.interval.Interval<? extends java.lang.Number>
     **/
    public static <T extends Number> Interval<T> getIntervalByValue(List<Interval<T>> intervals, Number value) {
        for (Interval<T> interval : intervals) {
            if (value instanceof Integer) {
                int intValue = value.intValue();
                if (intValue >= interval.getMin().intValue() && intValue <= interval.getMax().intValue()) {
                    return interval;
                }
            }
            if (value instanceof Long) {
                long longValue = value.longValue();
                if (longValue >= interval.getMin().longValue() && longValue <= interval.getMax().longValue()) {
                    return interval;
                }
            }
            if (value instanceof Double) {
                double doubleValue = value.doubleValue();
                if (doubleValue >= interval.getMin().doubleValue() && doubleValue <= interval.getMax().doubleValue()) {
                    return interval;
                }
            }
            if (value instanceof Float) {
                float floatValue = value.floatValue();
                if (floatValue >= interval.getMin().floatValue() && floatValue <= interval.getMax().floatValue()) {
                    return interval;
                }
            }
        }
        return null;
    }

    private IntervalUtils() {
        throw new UnsupportedOperationException();
    }

}
