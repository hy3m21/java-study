package com.dobest.lib.interval;

import lombok.Data;

/**
 * @author huangy
 * @since 2022/10/30 11:38 上午
 */
@Data
public class Interval<T> {

    private T min;

    private T max;

    private Interval(T min, T max) {
        this.min = min;
        this.max = max;
    }

    public static <T> Interval<T> from(T min, T max) {
        return new Interval<>(min, max);
    }

}
