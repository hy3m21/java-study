package com.dobest.file;

import cn.hutool.core.io.FileUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * 文件重命名工具
 *
 * @author huangy
 * @since 2022/11/4 10:43 下午
 */
public class FileRenameUtils {

    /**
     * 给文件批量重命名
     *
     * @param path 文件路径
     * @author huangy
     * @since 2022/11/4 10:54 下午
     **/
    public static void renameFileBatch(String path) throws IOException {
        Files.list(Path.of(path)).forEach(filePath -> {
            File file = new File(filePath.toString());
            String fileName = file.getName();
            System.out.println(fileName);
            if (fileName.contains(" [BD 1920x1080 23.976fps AVC-yuv444p10 FLAC Chap] - mawen1250")&& !fileName.contains("._")) {
                fileName = fileName.replace(" [BD 1920x1080 23.976fps AVC-yuv444p10 FLAC Chap] - mawen1250", "").replace(" [BD 1920x1080 23.976fps AVC-yuv444p10 FLAC Chap] v2 - mawen1250", "");
                FileUtil.rename(file, fileName, true);
            }
//            if (fileName.contains("._Hunter X Hunter 2011 - EP")) {
//                fileName = fileName.replace("._Hunter X Hunter 2011 - EP", "").replace(" [BD 1920x1080 23.976fps AVC-yuv444p10 FLAC Chap] v2 - mawen1250", "");
//            }
//            if (fileName.contains(" [BD 1920x1080 23.976fps AVC-yuv444p10 FLAC Chap] - mawen1250")) {
//                fileName = fileName.replace(" [BD 1920x1080 23.976fps AVC-yuv444p10 FLAC Chap] - mawen1250", "");
//            }
        });
    }

    public static void main(String[] args) throws IOException {
        renameFileBatch(FileConstants.RenameConstants.NEED_RENAME_FILE_PATH);
    }

    private FileRenameUtils() {
        throw new UnsupportedOperationException();
    }

}
