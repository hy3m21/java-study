package com.dobest.file;

/**
 * 文件常量
 *
 * @author huangy
 * @since 2022/11/4 10:44 下午
 */
public class FileConstants {

    /**
     * 重命名常量
     */
    public interface RenameConstants {
        //需要重命名的文件常量 文件夹名称
        String NEED_RENAME_FILE_PATH = "/Volumes/会思考的心/影视/动漫/日本/全职猎人2011[720]";

    }

}
