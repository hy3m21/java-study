package com.dobest.utils;

import cn.hutool.crypto.SecureUtil;

/**
 * @author huangy
 * @description
 * @since 2023/1/5 11:08
 **/
public class DesUtils {

    /*
     * @description 256加密
     * @author huangy
     * @since 2023/1/5 11:09
     * @param key
     * @return java.lang.String
     **/
    public static String sha256(String key) {
        return SecureUtil.sha256(key);
    }

}
