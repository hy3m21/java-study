package com.dobest.utils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author huangy
 * @description
 * @since 2022/11/17 17:47
 **/
public class PrintUtils {

    /*
     * @description 打印数组
     * @author huangy
     * @since 2022/11/19 11:12
     * @param array
     **/
    public static void printArray(byte[] array) {
        StringBuilder encodeMsg = new StringBuilder();
        encodeMsg.append("[");
        for (int i = 0; i < array.length; i++) {
            encodeMsg.append(array[i]);
            if (i < array.length - 1) {
                encodeMsg.append(",");
            }
        }
        encodeMsg.append("]");
        System.out.println(encodeMsg.toString());
    }

    /*
     * @description 打印list
     * @author huangy
     * @since 2022/11/17 18:01
     * @param list
     **/
    public static void printList(List<?> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
            if (i < list.size() - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        System.out.println(sb.toString());
    }

    /*
     * @description 打印map
     * @author huangy
     * @since 2022/11/17 18:01
     * @param map
     **/
    public static <K, V> void printMap(Map<K, V> map) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        AtomicInteger i = new AtomicInteger();
        int mapSize = map.size();
        map.forEach((k, v) -> {
            sb.append(k).append(":").append(v);
            if (i.get() < mapSize - 1) {
                sb.append(",");
            }
            i.getAndIncrement();
        });
        sb.append("]");
        System.out.println(sb.toString());
    }

    private PrintUtils() {
        throw new UnsupportedOperationException();
    }

}
