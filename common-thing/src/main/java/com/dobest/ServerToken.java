package com.dobest;

import lombok.Data;

/**
 * @author huangy
 * @description
 * @since 2023/2/16 16:46
 **/
@Data
public class ServerToken {

    //游戏编码
    private String gameCode;

    //服务器id
    private String gsId;

    //用户id
    private long userId;

    public ServerToken() {
    }

    private ServerToken(String gameCode, String gsId, long userId) {
        this.gameCode = gameCode;
        this.gsId = gsId;
        this.userId = userId;
    }

    public static ServerToken of(String gameCode, String gsId, long userId) {
        return new ServerToken(gameCode, gsId, userId);
    }

}
