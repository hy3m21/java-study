package org.example.reflect;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

/**
 * @author huangy
 * @since 2022/12/15 9:36 上午
 */
public class TestObj {

    private Set<Integer> ids;

    public TestObj() {
        ids = new HashSet<>();
        ids.add(1);
    }

    public boolean isContains(int id) throws NoSuchFieldException, IllegalAccessException {
        Field idsField = this.getClass().getDeclaredField("ids");
        Set<Integer> idSet = (Set<Integer>) idsField.get(this);
        return idSet.contains(id);
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        TestObj testObj = new TestObj();
        System.out.println(testObj.isContains(1));
    }

}
